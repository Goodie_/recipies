# Recipies

List of recipies that we will likely try again

* halumi wraps
* tomato-sauce meatballs
* mushroom cabinara
* noodle stirfry
* nachos
* burgers
* fish
* vegetarian curry
* butter chicken
* fajitas
    - https://www.easypeasyfoodie.com/easy-lamb-fajitas/
    - Probably best to only use 1/2 TP of cajun pepper
* dumplings
* mexican tofu with something
* fry up
* spag bowl
* pizza
* Steak
* Chicken pie
* Bacon and egg pie
* Chickpeas and hallumi
* Fried rice
* Miso salmon
* omlette


# A little more experimental
* [Spicy baked pasta with cheddar and broccoli](https://www.bonappetit.com/recipe/spicy-baked-pasta-with-cheddar-and-broccoli-rabe)
* [Cheesy cauliflower broccoli bake](https://diethood.com/garlicky-cheesy-cauliflower-broccoli-bake/)
